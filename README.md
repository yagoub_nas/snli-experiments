# snli-experiments

Experiments with the SNLI dataset, using lstm and transformers.

You can launch a training with automatic tensorboard logging with the following command:

`python src/train.py --model lstm --lr 0.0001 --epochs 10`

The model argument can be lstm or roberta