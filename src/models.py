import torch
import torch.nn as nn

from transformers import RobertaModel, RobertaConfig

from transformers import RobertaTokenizer
tokenizer = RobertaTokenizer.from_pretrained('roberta-base')

class LstmSNLI(nn.Module):
    def __init__(self):
        super(LstmSNLI, self).__init__()
        
        self.embedding = nn.Embedding(
            num_embeddings = tokenizer.vocab_size,
            embedding_dim = 128,
            padding_idx = tokenizer.pad_token_id,
            max_norm = 1
        )
        
        self.lstm = nn.LSTM(
            num_layers = 6,
            input_size = 128,
            hidden_size = 128,
            batch_first = True,
            dropout = 0.1,
            bidirectional = True
        )
        
        self.linear = nn.Linear(256, 3)
        
    def forward(self, input_ids):
        
        embed = self.embedding(input_ids)
        
        lstm_out = self.lstm(embed)
        
        return self.linear(lstm_out[0][:,0])

class RobertaSNLI(nn.Module):
    def __init__(self):
        super(RobertaSNLI, self).__init__()
        
        config = RobertaConfig.from_pretrained('roberta-base')
        
        config.max_position_embeddings = 130
        config.hidden_size = 256
        config.num_hidden_layers = 4
        config.intermediate_size = 512
        config.num_attention_heads = 4

        self.roberta = RobertaModel(config)

        self.roberta.requires_grad = True
        
        self.output = nn.Linear(256, 3)
        
        
    def forward(self, input_ids):
        
        roberta_out = self.roberta(input_ids)
        
        return self.output(roberta_out['pooler_output'])