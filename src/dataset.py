import torch

from datasets import load_dataset

snli = load_dataset("snli")
#Removing sentence pairs with no label (-1)
snli = snli.filter(lambda example: example['label'] != -1)

from transformers import RobertaTokenizer
tokenizer = RobertaTokenizer.from_pretrained('roberta-base')

bos_token = tokenizer.bos_token
sep_token = tokenizer.sep_token
eos_token = tokenizer.eos_token

def process_data(dataset):

    data = []

    for u in dataset:
        hypo = u['hypothesis']
        premise = u['premise']
        label = u['label']
        
        sentence = bos_token + ' ' + hypo + ' ' + sep_token + ' ' + premise + ' ' + eos_token
        
        sequence = tokenizer.encode(sentence, add_special_tokens = False, padding = 'max_length', max_length = 130)
        
        data.append((torch.tensor(sequence), torch.tensor(label)))

    return data

def get_dataloaders():
    train_data = process_data(snli['train'])
    valid_data = process_data(snli['validation'])
    test_data = process_data(snli['test'])

    return train_data, valid_data, test_data
