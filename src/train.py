import os
import argparse

import torch
import torch.nn as nn

from torch.utils.tensorboard import SummaryWriter

from tqdm import tqdm

from models import LstmSNLI, RobertaSNLI
from dataset import get_dataloaders

from transformers import RobertaTokenizer
tokenizer = RobertaTokenizer.from_pretrained('roberta-base')



def train(model, loader, f_loss, optimizer, device):
    """
    Train a model for one epoch, iterating over the loader
    using the f_loss to compute the loss and the optimizer
    to update the parameters of the model.

    Arguments :

        model     -- A torch.nn.Module object
        loader    -- A torch.utils.data.DataLoader
        f_loss    -- The loss function, i.e. a loss Module
        optimizer -- A torch.optim.Optimzer object
        device    -- a torch.device class specifying the device
                     used for computation

    Returns :
    """

    model.train()
        
    correct = 0
    tot_loss = 0
    N = 0
    
    iterator = tqdm(enumerate(loader))

    for _, (inputs, targets) in iterator:
        inputs, targets = inputs.to(device), targets.to(device)

        # Compute the forward pass through the network up to the loss
        outputs = model(inputs)
        loss = f_loss(outputs, targets)

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        tot_loss += inputs.shape[0] * f_loss(outputs, targets).item()
        
        predicted_targets = outputs.argmax(dim=1)
        correct += (predicted_targets == targets).sum().item()

        N += inputs.shape[0]
        
        iterator.set_description("loss : {: .3f} | accuracy : {: .3f}".format(tot_loss/N, correct/N))

    return tot_loss/N, correct/N


def test(model, loader, f_loss, device):
    """
    Train a model for one epoch, iterating over the loader
    using the f_loss to compute the loss and the optimizer
    to update the parameters of the model.

    Arguments :

        model     -- A torch.nn.Module object
        loader    -- A torch.utils.data.DataLoader
        f_loss    -- The loss function, i.e. a loss Module
        optimizer -- A torch.optim.Optimzer object
        device    -- a torch.device class specifying the device
                     used for computation

    Returns :
    """

    model.eval()
        
    correct = 0
    tot_loss = 0
    N = 0
    
    iterator = tqdm(enumerate(loader))

    for _, (inputs, targets) in iterator:
        inputs, targets = inputs.to(device), targets.to(device)

        # Compute the forward pass through the network up to the loss
        outputs = model(inputs)
        loss = f_loss(outputs, targets)
        
        tot_loss += inputs.shape[0] * f_loss(outputs, targets).item()
        
        predicted_targets = outputs.argmax(dim=1)
        correct += (predicted_targets == targets).sum().item()

        N += inputs.shape[0]
        
        iterator.set_description("loss : {: .3f} | accuracy : {: .3f}".format(tot_loss/N, correct/N))

    return tot_loss/N, correct/N


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--model", type=str, default='lstm',
                        help='Choose the type of model between lstm and roberta')
    parser.add_argument("--lr", type=float, default=0.00005,
                        help="Initial learning rate for the Adam optimizer")

    parser.add_argument("--train_batch_size", type=int, default=32,
                        help='Batch size for the training dataloader')
    parser.add_argument("--valid_batch_size", type=int, default=32,
                        help='Batch size for the validation dataloader')
    parser.add_argument("--epochs", type=int, default=10,
                        help='Number of training epochs')

    args = parser.parse_args()

    train_data, valid_data, test_data = get_dataloaders()

    train_loader = torch.utils.data.DataLoader(dataset=train_data,
                                               batch_size=args.train_batch_size,
                                               shuffle=True)

    valid_loader = torch.utils.data.DataLoader(dataset=valid_data,
                                               batch_size=args.valid_batch_size, 
                                               shuffle=False)
    test_loader = torch.utils.data.DataLoader(dataset=test_data,
                                              batch_size=args.valid_batch_size, 
                                              shuffle=False)


    if args.model == 'lstm':
        print("Loading LSTM model")
        model = LstmSNLI()
    else:
        print("Loading Bert-based model")
        model = RobertaSNLI()

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    model.to(device)

    f_loss = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr = 0.00005)

    run_dir = os.path.join(os.path.split(os.path.dirname(os.path.realpath(__file__)))[0], 'runs')

    writer = SummaryWriter(run_dir)

    for n_iter in range(args.epochs):
        print("EPOCH {}:".format(n_iter + 1))
        train_loss, train_acc = train(model, train_loader, f_loss, optimizer, device)
        valid_loss, valid_acc = test(model, valid_loader, f_loss, device)
        print("Train loss {: .3f} | Train accuracy {: .3f}".format(train_loss, train_acc))
        print("Valid loss {: .3f} | Valid accuracy {: .3f}\n".format(valid_loss, valid_acc))

        writer.add_scalars(
            '{}_small/Loss'.format(args.model),
            {'train' : train_loss, 'valid': valid_loss},
            n_iter
            )
        writer.add_scalars(
            '{}_small/Accuracy'.format(args.model),
            {'train' : train_acc, 'valid': valid_acc},
            n_iter)



if __name__ == '__main__':
    main()